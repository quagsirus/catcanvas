'''
use the following command to install the requirements: 'pip install -U -r requirements.txt'
or read requirements.txt and find those libraries elsewhere

This is a simple multiplayer maze game, you can play by yourself with WASD, running the included tests/webserver.py in a seperate window
and using IP: 127.0.0.1 and Port: 12346
Or you can run 2 instances of the game, selecting different ports using the 1st dialog box and connecting each instance to the other's port
after you've selected what port to host on (this starts the Flask web server you can see in the console)

ask me on teams if you can't get it working
'''

import os
import pygame
from flask import Flask, jsonify, request
import logging
from appJar import gui
import requests
from time import time
from threading import Thread

flask = Flask(__name__)

p1_path = 'p1.png'
p2_path = 'p2.png'

@flask.route('/pos', methods=['POST'])
def message_recieved(time_recieved=time()):
    data = request.json
    if data:  # if json data has been recieved, update player 2 position
        p2.x = data['x']
        p2.y = data['y']
    return jsonify({
        'r': time_recieved
    })  # Sends read receipt ( not used yet )

class Networking:
    def __init__(self, host_port):
        logging.getLogger('werkzeug').disabled = True  # Prevents every request from being logged
        flask.run(host='0.0.0.0', port=host_port)

    def update_postition(self, player):
        global running
        # puts player's coordinates into dictionary
        data = {
            'x': player.x,
            'y': player.y
        }
        try:
            requests.post('http://{ip}:{port}/pos'.format(ip=ip, port=str(port)), json=data, timeout=2.5)  # Sends json post request to previously entered server
        except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout):
            running = 'conn_lost'


class Cat(object):
    def __init__(self, img_path):
        # loads sprite from file
        self.sprite = pygame.image.load(img_path)
        # initial position ( top right )
        self.x = 75
        self.y = 75

    def handle_keys(self):
        key = pygame.key.get_pressed()  # currently pressed key
        dist = 3  # distance moved per frame
        if key[pygame.K_s]:  # down key
            self.y += dist  # move down
        elif key[pygame.K_w]:  # up key
            self.y -= dist  # move up
        if key[pygame.K_d]:  # right key
            self.x += dist  # move right
        elif key[pygame.K_a]:  # left key
            self.x -= dist  # move left

    def draw(self, surface):
        # blit self  at current position
        surface.blit(self.sprite, (self.x, self.y))

class Maze:
    def __init__(self):
       self.M = 10
       self.N = 8
       self.maze = [ 1,1,1,1,1,1,1,1,1,1,
                     1,0,0,0,0,0,0,0,0,1,
                     1,0,0,0,0,0,0,0,0,1,
                     1,0,1,1,1,1,1,1,0,1,
                     1,0,1,0,2,0,0,0,0,1,
                     1,0,1,0,1,1,1,1,0,1,
                     1,0,0,0,0,0,0,0,0,1,
                     1,1,1,1,1,1,1,1,1,1,]

    def draw(self, surface):
        bx = 0
        by = 0
        for _ in range(0,self.M*self.N):
            if self.maze[ bx + (by*self.M) ] == 1:
                pygame.draw.rect(surface, (255, 0, 0), (bx * 70, by * 70, 70, 70))  # Draws rectangle for every 1 in array
            elif self.maze[ bx + (by*self.M) ] == 2:
                pygame.draw.rect(surface, (0, 255, 255), (bx * 70, by * 70, 70, 70))  # Draws rectangle for every 1 in array

            bx = bx + 1
            if bx > self.M-1:
                bx = 0 
                by = by + 1

# Creates pygame window
pygame.init()
screen = pygame.display.set_mode((800, 800))

p1 = Cat(p1_path)  # create player 1 character
p2 = Cat(p2_path)  # create player 2 character
maze = Maze()
clock = pygame.time.Clock()  # Starts internal clock

aj = gui()  # Creates an appJar instance

# Choice of 2 flask ports to host on
host_port = aj.questionBox('internal server', 'Would you like to use the host port (12345)? 54321 will be used if not.')
if host_port:
    host_port = 12345
else:
    host_port = 54321

# Starts flask server as child ( it is killed with the parent )
net = Thread(target=Networking, args=[host_port])
net.daemon = True
net.start()

# Obtain server info to send own position to
ip = aj.stringBox('ip', 'peer ip address')
if not ip:
    ip = '127.0.0.1'
port = aj.integerBox('port', 'peer port')
if port == 0:
    port = '12346'

# Generate winning text
font = pygame.font.SysFont(None, 24)
win_txt = font.render('You win!', True, pygame.Color(75, 75, 0))
lose_txt = font.render('You lose!', True, pygame.Color(75, 75, 0))

win = False

running = True
iterations = 0
while running == True:  # Runs every frame
    # Broadcasts p1 position every 10 frames
    iterations += 1
    if iterations == 10:
        iterations = 0
        Networking.update_postition(Networking, p1)
    
    # handle every event since the last frame.
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()  # quit the screen
            running = False

    p1.handle_keys()  # handle inputs

    # Collision handling for both players
    for player in [p1, p2]:
        for color in [pygame.Color(0, 255, 255), pygame.Color(255, 0, 0)]:  # For both cyan and red
            try:
                if color in [screen.get_at([player.x-1, player.y-1]), screen.get_at([player.x+51, player.y+51]),
                                            screen.get_at([player.x+51, player.y-1]), screen.get_at([player.x-1, player.y+51])]:
                    if color == pygame.Color(0, 255, 255):  # Player reached flag
                        if player == p1:
                            win = 'p1'
                        else:
                            win = 'p2'
                        p1.x = 75
                        p1.y = 75
                        p2.x = 75
                        p2.y = 75
                    else:
                        raise IndexError
            except IndexError:  # Player touching walls or out of bounds
                player.x = 75
                player.y = 75

    screen.fill((255,255,255))  # fill the screen with white
    maze.draw(screen)
    p2.draw(screen)  # draw player 2 to the screen
    p1.draw(screen)  # draw player 1 to the screen ( your player on top )
    if win == 'p1':
        screen.blit(win_txt, (150, 75))
    elif win == 'p2':
        screen.blit(lose_txt, (150, 75))
    pygame.display.update()  # update the screen

    clock.tick(60)  # maximum framerate
if running == 'conn_lost':
    aj.infoBox('conn_lost', '＞︿＜  Connection to peer timed out')
