import requests
from flask import Flask, jsonify, request
from time import time
import logging

flask = Flask(__name__)

@flask.route('/pos', methods=['POST'])
def message_recieved(time_recieved=time()):
    data = request.json
    if data:
        print(data)
    return jsonify({
        'r': time_recieved
    })

logging.getLogger('werkzeug').disabled = True
flask.run(port=12346)
